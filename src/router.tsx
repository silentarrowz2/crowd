import React from "react"
import { BrowserRouter as Router, Route, } from "react-router-dom";
import OrdersList from './component/OrdersList';

const Routes = () => {
    return(
        <Router>            
            <Route exact path='/' component={OrdersList} />     
            <Route exact path='/orders' component={OrdersList} />         
        </Router>
    )
}

export default Routes;