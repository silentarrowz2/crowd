import React from 'react';
import { Layout, Row, Col, Tabs } from "antd";
import PageHeader from '../PageHeader';
import DisplayChart from '../common/DisplayChart';
import DisplayTabs from '../common/DisplayTabs';

const { Content } = Layout;

const OrdersList = () => {
    return(
        <div className="contentWrapper">
          <div className="content">
            <PageHeader />
            <Row gutter={16} className="rowWrapper">
              <Col xl={6} lg={6} md={6} sm={24}>
                <div className="infoWrapper">
                  <div className="cardInfo">
                    <span>Active Orders</span>                   
                    <span>
                      <strong>624</strong>
                    </span>
                  </div>
                  <div>
                    <DisplayChart stroke='#8884d8' fill='#8884d8' />
                  </div>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={24}>
                <div className="infoWrapper">
                  <div className="cardInfo">
                    <span>Unfulfilled</span>                    
                    <span>
                      <strong>159</strong>
                    </span>
                  </div>
                  <div>
                    <DisplayChart stroke='#E5AE43' fill='#E5AE43' />
                  </div>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={24}>
                <div className="infoWrapper">
                  <div className="cardInfo">
                    <span>Pending Receipts</span>
                    <span>
                      <strong>624</strong>
                    </span>
                  </div>
                  <div>
                    <DisplayChart stroke='#894FAC' fill='#894FAC' />
                  </div>
                </div>
              </Col>
              <Col xl={6} lg={6} md={6} sm={24}>
                <div className="infoWrapper">
                  <div className="cardInfo">
                    <span>Fulfilled</span>                   
                    <span>
                      <strong>263</strong>
                    </span>
                  </div>
                  <div>
                    <DisplayChart stroke='#B2F9DF' fill='#B2F9DF' />
                  </div>
                </div>
              </Col>
            </Row>
            <Row justify="center">
              <Col md={24}>
                <DisplayTabs />
              </Col>
            </Row>
          </div>
        </div>
    )
}

export default OrdersList;