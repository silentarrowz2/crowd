import React from 'react';
import { Layout, Row, Col, Tabs } from "antd";
import DisplayInfoTable from '../DisplayInfoTable';

const { TabPane } = Tabs;


const DisplayTabs: React.FC<{}> = () => {

    function callback(key:string) {
        console.log(key);
    }

    return(
        <Tabs defaultActiveKey="1" onChange={callback}>
            <TabPane tab="All Orders" key="1">
                <DisplayInfoTable />
            </TabPane>
            <TabPane tab="Active" key="2">
                <DisplayInfoTable />
            </TabPane>
            <TabPane tab="Unpaid" key="3">
                <DisplayInfoTable />
            </TabPane>
            <TabPane tab="Unfulfilled" key="4">
                <DisplayInfoTable />
            </TabPane>                  
        </Tabs>  
    )
}

export default DisplayTabs;