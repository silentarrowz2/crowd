import React from "react";
import { Tag, Table } from "antd";
import { ColumnsType } from 'antd/es/table';

enum Fulfillment {
  FULFILLED = "Fulfilled",
  UNFULFILLED = "Unfulfilled",
  PENDING_RECEIPT = "Pending Receipt",
}

enum Status {
  AUTHORIZED = "Authorized",
  PAID = "Paid",
}

interface Data {
  key: string;
  orderid: string;
  created: string;
  customer: string;
  fulfillment: string | Fulfillment;
  profit: number;
  status: string | Status;
  total: number;
  updated: string;
}

const dataSource: Data[] = [
  {
    key: "1",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Unfulfilled",
    total: 32000,
    profit: 1000,
    status: "Authorized",
    updated: "today",
  },
  {
    key: "2",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Fulfilled",
    total: 32000,
    profit: 1000,
    status: "Paid",
    updated: "today",
  },
  {
    key: "3",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Fulfilled",
    total: 32000,
    profit: 1000,
    status: "Authorized",
    updated: "today",
  },
  {
    key: "4",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Pending Receipt",
    total: 32000,
    profit: 1000,
    status: "Authorized",
    updated: "today",
  },
  {
    key: "5",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Fulfilled",
    total: 32000,
    profit: 1000,
    status: "Paid",
    updated: "today",
  },
  {
    key: "6",
    orderid: "124",
    created: "12/10/20",
    customer: "Mike",
    fulfillment: "Unfulfilled",
    total: 32000,
    profit: 1000,
    status: "Paid",
    updated: "today",
  },
];

const columns:ColumnsType<Data> = [
  {
    title: "OrderId",
    dataIndex: "orderid",
    key: "orderid",
  },
  {
    title: "Created",
    dataIndex: "created",
    key: "created",
  },
  {
    title: "Customer",
    dataIndex: "customer",
    key: "customer",
  },
  {
    title: "Fulfillment",
    dataIndex: "fulfillment",
    key: "fulfillment",
    render: (text: string): React.ReactNode => {
      let color = "";
      if (text === Fulfillment.UNFULFILLED) {
        color = "rgb(254, 168,1)";
      } else if (text === Fulfillment.FULFILLED) {
        color = "rgb(57, 195, 142)";
      } else if (text === Fulfillment.PENDING_RECEIPT) {
        color = "rgb(141, 57, 195)";
      }
      return <Tag color={color}> {text} </Tag>;
    },
  },
  {
    title: "Total",
    dataIndex: "total",
    key: "total",
    render: (text: string): string => "$" + text,
  },
  {
    title: "Profit",
    dataIndex: "profit",
    key: "profit",
    render: (text: string): string => "$" + text,
  },
  {
    title: "Status",
    dataIndex: "status",
    key: "status",
    render: (text: string): React.ReactNode => {
      let color = "";
      if (text === Status.AUTHORIZED) {
        color = "rgb(254, 168,1)";
      } else if (text === Status.PAID) {
        color = "rgb(57, 195, 142)";
      }
      return <Tag color={color}> {text} </Tag>;
    },
  },
  {
    title: "Updated",
    dataIndex: "updated",
    key: "updated",
  },
];

const DisplayInfoTable: React.FC<{}> = () => {
  const rowSelection:{} = {
    onChange: (selectedRowKeys: number, selectedRows: []) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        "selectedRows: ",
        selectedRows
      );
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === "Disabled User", // Column configuration not to be checked
      name: record.name,
    }),
  };

  return (
    <Table
    rowSelection={{
      type: "checkbox",
      ...rowSelection,
    }}
    dataSource={dataSource}
    columns={columns}
    pagination={false}
    />
  );
};

export default DisplayInfoTable;
