import React from "react";
import { Button } from "antd";
import './styles.css';

const PageHeader: React.FC<{}> = () => {
  return (
    <div className="pageHeader">
      <h3>Orders List</h3>
      <div>
        <Button type="primary">Create Order</Button>
      </div>
    </div>
  );
};

export default PageHeader;
