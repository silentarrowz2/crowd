import React from "react";
import { Avatar } from 'antd';
import { BellFilled, UserOutlined, CodepenOutlined } from '@ant-design/icons';
import { BrowserRouter as Router, Link, NavLink } from "react-router-dom";
import './styles.css';

const Navbar: React.FC<{}> = () => {
    const activeStyle = { color: "rgb(32,96,218)", height:'40px',borderBottom:'1px solid rgb(32,96,218)'};

    return (
        <div className='header'>
        <div className='headerContent' >
        <div>
        <CodepenOutlined style={{color:'rgb(30,97,220)', marginTop:'5px', fontSize: '25px'}} />
        </div>

        <div className='links'>
          <Router>
            <NavLink activeStyle={activeStyle} to='/dashboards'>Dashboards</NavLink>
            <NavLink activeStyle={activeStyle} to='/orders'>Orders</NavLink>
            <NavLink activeStyle={activeStyle} to='/customers'>Customers</NavLink>
            <NavLink activeStyle={activeStyle} to='/inventory'>Inventory</NavLink>
            <NavLink activeStyle={activeStyle} to='/settings'>Settings</NavLink>
          </Router>
        </div>

        <div className='user'>
          <BellFilled className='notifIcon' />
          <Avatar style={{ backgroundColor: '#87d068' }} size='small' icon={<UserOutlined />} />
        </div>
        </div>
      </div>
    )
}

export default Navbar;