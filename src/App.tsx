import React from "react";
import { Layout, } from "antd";
import Routes from "./router";
import Navbar from './component/Navbar';
import "./App.css";
import "antd/dist/antd.css";

const App: React.FC<{}> = () => {

  return (
    <div className="App">
      <Layout style={{ height: "100%" }}>
        <Navbar />
        <Routes />
      </Layout>
    </div>
  );
};

export default App;
